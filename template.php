<?php

function nhd_theme_field($variables) {
  $output = '';

  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<h3>' . $variables['label'] . ':</h3> ';
  }

  // Render the items.
  foreach ($variables['items'] as $delta => $item) {
    $output .= '<div class="field-item">' . drupal_render($item) . '</div>';
  }

  // Render the top-level DIV.
  $output = '<div class="field">' . $output . '</div>';

  return $output;
}